from django.shortcuts import render,redirect
from.models import *
from.forms import *
from django.contrib.auth import login
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.core.mail import send_mail
from django.utils import timezone
import random
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404


"######ACCOUNT SEACTION##############"
def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = True
            user.save()
            
            # Send confirmation email
            send_mail(
                subject='Thank you for registering',
                message='Thank you for signing up with our website! We are glad to have you.',
                from_email=settings.DEFAULT_FROM_EMAIL,
                recipient_list=[user.email],
                fail_silently=False,
            )

            return redirect('Login')
        else:
            print(form.errors)  
            
    else:
        form = RegistrationForm()

    return render(request, 'signup.html', {'form': form})


def home(request):
    return render(request,'index.html')




def aboutus(request):
    return render(request,'About.html')


def user_login(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return redirect('home') 
            else:
                return render(request, 'Login.html', {'error': 'Your account is disabled.'})
        else:
            return render(request, 'Login.html', {'error': 'Invalid username or password.'})
    else:
        return render(request, 'Login.html')
    
    
def user_list(request):
    users = Account.objects.all()  
    return render(request, 'user_list.html', {'users': users})
    



def contactus(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            contact_query = form.save(commit=False)
            contact_query.save()
            message = f"From: {contact_query.first_name} {contact_query.last_name}\nEmail: {contact_query.email}\n\n{contact_query.query}"
            send_mail(
                subject='New Contact Us Query',
                message=message,
                from_email=contact_query.email,
                recipient_list=[settings.EMAIL_HOST_USER],  
                fail_silently=False,
            )
            messages.success(request, "Thank you for contacting us!")
            return redirect('login')
    else:
        form = ContactForm()
    return render(request, 'ContactUs.html', {'form': form})



@csrf_exempt  
def send_otp(request):
    if request.method == 'POST':
        form1 = SendOTPForm(request.POST)
        if form1.is_valid():
            email = form1.cleaned_data['email']
            user = Account.objects.filter(email=email).first()
            if user:
                otp = ''.join(random.choice('0123456789') for _ in range(6))  
                request.session['otp'] = otp
                request.session['otp_email'] = email
                send_otp_email(email, otp)
                
                return JsonResponse({'success': True, 'message': 'OTP sent successfully.'})
            else:
                return JsonResponse({'success': False, 'error': 'Email not registered'}, status=404)
        else:
            errors = form1.errors.as_json()
            return JsonResponse({'success': False, 'error': 'Invalid form data', 'form_errors': errors}, status=400)
    else:
        return JsonResponse({'success': False, 'error': 'Invalid request'}, status=405)
def send_otp_email(email, otp):
    subject = 'OTP for Password Reset'
    message = f'Your OTP for password reset is: {otp}'
    send_mail(subject, message, settings.EMAIL_HOST_USER, [email])

def verify_otp(request):
    if request.method == 'POST':
        otp_entered = request.POST.get('otp')
        stored_otp = request.session.get('otp')
        email = request.POST.get('email')
        if otp_entered == stored_otp:
            request.session['reset_email'] = email
            return render(request, 'forgot_password.html')
        else:
            messages.error(request, 'Invalid OTP')
    return redirect('send_otp')

def forgot_password(request):
    if request.method == 'POST':
        form = PasswordResetRequestForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirm_password']
            if password == confirm_password:
                email = request.session.get('reset_email')
                user = Account.objects.filter(email=email).first()
                if user:
                    user.set_password(password)
                    user.save()
                    del request.session['otp']
                    del request.session['otp_email']
                    del request.session['reset_email']
                    messages.success(request, 'Password reset successful. You can now login with your new password.')
                    return redirect('Login')
            else:
                messages.error(request, 'Passwords do not match')
    else:
        form = PasswordResetRequestForm()
    return render(request, 'Forget.html', {'form': form})

"#####END OF ACCOUNT SEACTION######"   



"######PRODUCT SEACTION START###########" 
def add_product(request):
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('product_list')  
    else:
        form = ProductForm()
    return render(request, 'add_product.html', {'form': form})

def product_list(request):
    products = Product.objects.all()
    return render(request, 'product_list.html', {'products': products})

def product_detail(request, product_id):
    product = get_object_or_404(Product, id=product_id)
    return render(request, 'product_detail.html', {'product': product})


def cart_detail(request):
    cart = request.session.get('cart', {})
    products = Product.objects.filter(id__in=cart.keys())
    cart_items = [(product, cart[str(product.id)]) for product in products]
    return render(request, 'cart_detail.html', {'cart_items': cart_items})


def add_to_cart(request, product_id):
    cart = request.session.get('cart', {})
    if product_id in cart:
        cart[product_id] += 1
    else:
        cart[product_id] = 1
    request.session['cart'] = cart
    return redirect('product_list')


